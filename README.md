## This is the [KROME](https://bitbucket.org/tgrassi/krome/overview) repository.

KROME is a nice and friendly package to model chemistry and microphysics 
 for a wide range of astrophysical simulations. 
 Given a chemical network (in CSV-like format) it automatically 
 generates all the routines needed to solve the kinetic of the system, 
 modelled as system of coupled Ordinary Differential Equations. 
 It provides different options which make it unique and very flexible. 
 Any suggestions and comments are welcomed. KROME is an open-source 
 package, GNU-licensed, and any improvements provided by 
 the users is well accepted. See disclaimer below and GNU License 
 in gpl-3.0.txt.


KROME is available on 

- [http://www.kromepackage.org](http://www.kromepackage.org)

 and

- [https://bitbucket.org/tgrassi/krome](https://bitbucket.org/tgrassi/krome)

---

you can quickly clone by:
```
git clone https://bitbucket.org/tgrassi/krome.git
```

---

To get support or receive news about KROME please refer to our user mailing list: 

 - https://groups.google.com/forum/#!forum/kromeusers


More information on the wiki

 - https://bitbucket.org/tgrassi/krome/wiki/Home

---

Written and developed by Tommaso Grassi
```
 tommasograssi@gmail.com
 Starplan, Copenhagen.
 Niels Bohr Institute, Copenhagen.
```

Co-developer Stefano Bovino
```
 stefano.bovino@uni-hamburg.de
 Hamburger Sternwarte, Hamburg.
```

Contributors (alphabetically): D. Galli, F.A. Gianturco, T.Haugbølle, J. Prieto,
 J. Ramsey, D.R.G. Schleicher, D. Seifried, E. Simoncini, E. Tognelli 

---

KROME is provided "as it is", without any warranty. 
 The Authors assume no liability for any damages of any kind 
 (direct or indirect damages, contractual or non-contractual 
 damages, pecuniary or non-pecuniary damages), directly or 
 indirectly derived or arising from the correct or incorrect 
 usage of KROME, in any possible environment, or arising from 
 the impossibility to use, fully or partially, the software, 
 or any bug or malfunction.
 Such exclusion of liability expressly includes any damages 
 including the loss of data of any kind (including personal data)

---

Additional notes: this version of KROME is a developer version,
 while the stable version has been dropped. For this reason we
 warmly recommend to check the status of the release by using the
 test website
 http://kromepackage.org/test/

 More information on the "tested" version here
 https://bitbucket.org/tgrassi/krome/wiki/stable_version